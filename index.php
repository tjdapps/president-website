<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="Automotive Technology at Las Positas College (LPC)" />
<meta name="keywords" content="Automotive Technology, Las Positas College, Las Positas, LPC" />
<?php 
//define('root', 'http://' . $_SERVER['SERVER_NAME'] . '/');
define('root', $_SERVER['DOCUMENT_ROOT'] . '/');
?>
<title>Automotive Technology - Las Positas College</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />


<link href="/files/css/home/base.css" rel="stylesheet" type="text/css" />
<link href="/files/css/home/lpc-bootstrap.css" rel="stylesheet" type="text/css" />
<link href="/files/css/home/contact-us.css" rel="stylesheet" type="text/css" />
<link href="/files/css/home/left-nav.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/files/css/auto/page.css" type="text/css" />

<style type="text/css">

</style>

<link href='https://fonts.googleapis.com/css?family=Oswald:400,700|Open+Sans:400,400italic,700italic' rel='stylesheet' type='text/css'>

</head>

<body>

<?php include_once(root . "files/php/ga/googletags.php"); ?>

<!-- HEADER -->

<?php include_once(root. "files/includes/header-home-bootstrap.inc") ?>

<!-- end of HEADER -->

<div class="container">
<div class="row">
  <div id="content-area" tabindex="-1">


    <div class="col-md-9 col-md-push-3">

	    <div class="row">
		  	<div class="col-md-8">
		  		<h1>Automotive Technology</h1>
		  		
		  		<p>Auto service departments increasingly need people with technical training in electronics. Cars today have changed because mechanical systems have become far more reliable. Car problems are more likely to be caused by a defective electronic sensor than a bad transmission. If you "bleed motor oil" Las Positas College gives you the training to work with the latest automotive technology, whether in the shop or as the start to an exciting career in the automotive industry.</p>

				<p>The Automotive Technology program is designed for immediate job entry with opportunities to build more advanced certification leading up to the AS Degree.</p>

				<div id="ged" class="word-break">
					<h2>Federal Gainful Employment Disclosure</h2>
				 	<p>Use the link below to view Las Positas Community Colleges Federal Gainful Employment Disclosure for this program.</p>
					
					<ul class="fa-ul">
						<li><span class="fa-li fa fa-arrow-right"></span> <a href="/ged/autotech/gedt.html">Certificate of Achievement in Automotive Technician</a></li>
						<li><span class="fa-li fa fa-arrow-right"></span> <a href="/ged/autosvctech/gedt.html">Certificate of Achievement in Automotive Service</a></li>
					</ul>
				 	
				 	
				</div> 

		  	</div>
	  	
		  	<div class="col-md-4">
		  		<div class="panel panel-default contact-info">
					<div class="panel-heading">
						<h3 class="panel-title">Contact Information</h3>
					</div>
					<div class="panel-body word-break">
				    	<p><strong>Brian Hagopian</strong></p>

				    	<p>
				    	<strong>Phone:</strong><br /><a href="tel:925-424-1171">925-424-1171</a></p>
						<p><strong>Email:</strong><br /><a href="mailto:bhagopian@laspositascollege.edu">bhagopian@laspositascollege.edu</a></p>
					</div>
				</div>
		  	</div>

	    </div>

  	</div>

  	<div class="col-md-3 col-md-pull-9">
	  	<ul id="left-navigation" class="nav nav-pills nav-stacked">
	  		<li class="active"><a href="index.php"><span class="fa fa-chevron-right"></span>Automotive Technology</a></li>
	  		<li><a href="start.php"><span class="fa fa-chevron-right"></span>Getting Started</a></li>
	  		<li><a href="program.php"><span class="fa fa-chevron-right"></span>The Program</a></li>
	  		<li><a href="crossover.php"><span class="fa fa-chevron-right"></span>Crossover Courses</a></li>
	  		<li><a href="jobs.php"><span class="fa fa-chevron-right"></span>The Job Market</a></li>
	  		<li><a href="transfer.php"><span class="fa fa-chevron-right"></span>Transfer Opportunities</a></li>
	  		<li><a href="similar-programs.php"><span class="fa fa-chevron-right"></span>Similar Programs</a></li>
	  		<li><a href="faculty.php"><span class="fa fa-chevron-right"></span>Our Faculty</a></li>
	  		<li><a href="/careereducation/index.php"><span class="fa fa-chevron-right"></span>Career &amp; Technical Education</a></li>
	  		<li><a href="/programs/index.php"><span class="fa fa-chevron-right"></span>All Programs</a></li>
	  	</ul>
  	</div>
  


  </div>
</div>
</div>
  
<!-- Start Footer -->

<?php include_once (root . "files/includes/footer-home-bootstrap.inc"); ?>

<!-- End Footer--> 

<!-- <div class="container-fluid">
  Current viewport width:<span id="monitor"></span>
</div> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<script type='text/javascript'>        
$(document).ready(function() {        
  $('#monitor').html($(window).width());

  $(window).resize(function() {
      var viewportWidth = $(window).width();
      $('#monitor').html(viewportWidth);
  });
});       
</script>

</body>
</html>